
// Reload table
function qviews_reload(unique_id, pid, args, url) {
  $("#qviews_feed_" + unique_id).animate({opacity:0.4},400);
  // Position the loading div and show it
  loading_height = $("#qviews_loading_" + unique_id).height();
  loading_width = $("#qviews_loading_" + unique_id).width();
  table_width = $("#qviews_feed_" + unique_id).width();
  table_height = $("#qviews_feed_" + unique_id).height();
  loading_margin_top = table_height/2;
  loading_margin_top = loading_margin_top - loading_margin_top - loading_margin_top;
  $("#qviews_loading_" + unique_id).css("margin-top",(table_height-20)-(table_height-20)-(table_height-20) + 'px');
  $("#qviews_loading_" + unique_id).css("margin-left",((table_width/2)-(loading_width/2)) + 'px');
  $("#qviews_loading_" + unique_id).show();

  // Compile filters
  var filter_query = '';
  $('.qviews_filter_' + unique_id).each(function() {
    type = $(this).attr('type');
    if (type == 'text' || type == 'select') {
      filter_query = '&' + filter_query + '&' + $(this).attr('id') + '=' + $(this).val();
    } else if (type == 'checkbox') {
      filter_query = '&' + filter_query + '&'+ $(this).attr('id') + '=' + $(this).is(':checked');
    }
  });
  $.ajax({
    url: url,
    type: "POST",
    data: "action=reload&unique_id=" + unique_id + "&pid=" + pid + "&q=" + $("#qviews_url_" + unique_id).val() + "&page=" + $("#qviews_page_input_" + unique_id).val() + "&search=" + $("#qviews_search_input_" + unique_id).val() + "&order=" + $("#qviews_order_input_" + unique_id).val() + "&args=" + args + filter_query,
    error: function(){
      //alert("There was a problem with reloading");
    },
    success: function(msg){
      $("#qviews_loading_" + unique_id).hide();
      $("#qviews_feed_" + unique_id).animate({opacity:1},400);
      eval("qviews_reloaded_" + unique_id + "()");
      $("#qviews_feed_" + unique_id).html(msg);
      // This runs through and loads any javascript functions after loading. In later versions
      // of jquery, I can use $(document).ready() to achieve the functionality much more simply.

      // Firefox seems to process new javascript just fine without this. IE7 and 6 (the ones I tested) do not
      if (($.fn.jquery) < 1.1 || $.browser.msie) {
        $(".qviews-script-reload").each(function() {
          //eval($(this).text());
          eval($(this).html());
        });
      }
      $(".qviews_pager_container_" + unique_id).html($("#qviews_new_pager_" + unique_id).html());
      //$(".datepicker_input").datepicker();
    }
  });
}


function qviews_load_callback(select_id, input_name, help_id) {
  if ($("#" + select_id).val() != "") {
    $('#' + input_name + '_args_outer').hide();
   // alert($("input[@name='" + input_name + "_args']").val());
    $("textarea[@name='" + input_name + "_args']").val($("#" + input_name + $("#" + select_id).val() + "_args_content").text());
    $("textarea[@name='" + input_name + "_args']").html($("#" + input_name + $("#" + select_id).val() + "_args_content").text());
    $('#' + input_name + '_args_outer').show();
    $("input[@name='" + input_name + "']").val($("#" + select_id).val());
    $("#" + input_name + "_help").html($("#" + input_name + $("#" + select_id).val() + "_help_content").html());
  }
}


function qviews_column_change(col_input) {
  if ($(col_input).val() != '') {
    if ($('#grid_callback_outer').css('display') == 'none') {
      $('#grid_callback_outer').show('slow');
    }
    if ($('#column_options').css('display') != 'none') {
      //$('#column_options').hide('slow');
    }
  } else {
    if ($('#grid_callback_outer').css('display') != 'none') {
      $('#grid_callback_outer').hide('slow');
    }
    if ($('#column_options').css('display') == 'none') {
      //$('#column_options').show('slow');
    }
  }
}

function qviews_input_to_textarea(args_input, rows, cols) {
  if ($('input[@name=' + args_input + ']').attr('type') == 'text') {
    $('input[@name=' + args_input + ']').parent().html('<textarea type="textarea" cols="' + cols + '" rows="' + rows + '" id="' + args_input + '" name="' + args_input + '">' + $('input[@name=' + args_input + ']').val() + '</textarea>');
  } else {
    $('#' + args_input).parent().html('<input type="text" size="' + cols + '" name="' + args_input + '" value="' + $('#' + args_input).val() + '"></input>');
    $('#' + args_input).parent().val('<input type="text" size="' + cols + '" name="' + args_input + '" value="'+ $('#' + args_input).val() + '"></input>');
  }
}

function qviews_process_ajax_col(input_type, input_id, callback, url, table, primary_col, col) {
  var query;
  var input = $('#' + input_id);
  var loading = $('#' + input_id + '_loading');
  $('#' + input_id + '_loading').html('<span>Loading...</span>');
  if (input_type == 'checkbox') {
    query = 'type=checkbox&checked=' + $('#' + input_id).is(':checked');
  }
  if (input_type == 'select') {
    query = 'type=select&value=' + $('#' + input_id).val();
  }
  if (input_type == 'text') {
    query = 'type=text&value=' + $('#' + input_id + '_textarea').val();
  }
  if (input_type == 'date') {
    //alert($('#' + input_id +'_convert_to_timestamp').val());
    //alert($('#' + input_id + '_date_hidden').val());
    //alert($('#' + input_id + '_date_input').val());
    query = 'type=date&value=' + $('#' + input_id + '_date_hidden').val() + '&time=' + $('#' + input_id + '_time').text() + '&convert_to_timestamp=' + $('#' + input_id +'_convert_to_timestamp').val() + '&seconds=' + $('#' + input_id + '_seconds').text();
  }
  $.ajax({
    url: url,
    type: "POST",
    data: "action=ajax_col_change&input_id=" + input_id + "&callback=" + callback + "&" + query + "&primary_col=" + primary_col + "&table=" + table + '&key=' + $('#' + input_id + '_key').val() + '&values=' + $('#' + input_id + '_values').val() + '&col=' + col,
    error: function(msg){
      $('#' + input_id + '_loading')('<span title="' + msg + '">Error</span>');
    },
    success: function(msg){
      $('#' + input_id + '_loading').html('<span>Done</span>');
    }
  });
}

function qviews_switch_text_textarea(input_id) {
  if ($("#" + input_id + "_outer").html() == $("#" + input_id + "_content").html()) {
    $("#" + input_id + "_outer").html('<textarea cols="30" rows="5">' + $("#" + input_id + "_full_content").html() + "</textarea>");
  } else {
    $("#" + input_id + "_outer").html($("#" + input_id + "_content").html());
  }
}

function qviews_text_col_save(input_id) {
  var textarea_val = $('#' + input_id + '_textarea').val();
  $('#' + input_id + '_full').html($('#' + input_id + '_outer').html());
  $('#' + input_id + '_full > textarea').html(textarea_val);
  $('#' + input_id + '_outer').html('<span>' + $('#' + input_id + '_textarea').val() + $('#' + input_id + '_edit_link').html() + '</span>');
}

function qviews_clear_search(input_id) {
  $('.qviews_filter_' + unique_id).each(function() {
    type = $(this).attr('type');
    if (type == 'text' || type == 'select') {
      $(this).val('');
    } else if (type == 'checkbox') {
      $(this).attr('checked', '');
    }
  });
  $("#qviews_search_input_" + unique_id).val('');
}
function qviews_custom_row_change(num) {
   if ($(".custom_cols_outer_" + (num+1)).css("display") == "none") {
    $(".custom_cols_outer_" + (num+1)).slideDown();
  }
  //if ($(".custom_code_cols_" + num).val() == "" && $(".custom_cols__outer_" + (num+1)).css("display") != "none") {
    //$(".custom_cols_outer_" + (num+1)).slideUp();
  //}
}
function qviews_filter_change(num) {
  if ($(".filter_types_" + num).val() == "checkbox") {
    $(".filter_param_help_" + num).html('<div class="qviews_callback_help">For a checkbox input, you need to set what value the checkbox is when checked. Put that value in the "Parameters" input. You also need to select a comparison, though it does not matter which one.</div>');
  } else if ($(".filter_types_" + num).val() == "date") {
    $(".filter_param_help_" + num).html('<div class="qviews_callback_help">The date column assumes that the input is a timestamp. If it is not, please supply the date format in the "Parameters" input.<ul><li>d - day of month (no leading zero)</li><li>dd - day of month (two digit)</li><li>D - day name short</li><li>DD - day name long</li><li>m - month of year (no leading zero)</li><li>mm - month of year (two digit)</li><li>M - month name short</li><li>MM - month name long</li><li>y - year (two digit)</li><li>yy - year (four digit)</li></ul></div>');
  } else {
    $(".filter_param_help_" + num).html("<span></span>");
  }
  if ($(".filter_columns_" + num).val() != "" && $(".filter_outer_" + (num+1)).css("display") == "none") {
    $(".filter_outer_" + (num+1)).slideDown();
  }
  if ($(".filter_columns_" + num).val() == "" && $(".filter_outer_" + (num+1)).css("display") != "none") {
    $(".filter_outer_" + (num+1)).slideUp();
  }
}

function qviews_form_menu() {
    $('legend[@class!=nomenu]').after('<div class="qviews-top-link"><a href="#form-top">Menu</a> | <a href="#save">Submit</a></div>');
    $('legend[@class!=nomenu]').addClass('nomenu');
}