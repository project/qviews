<?php


// These values are taken from the Q-Views export tool.
$feed["pid"] = "Q-Views";
$feed["description"] = "A list of qviews feeds.";
$feed["blocks"] = "0";
$feed["module"] = "qviews";
$feed["path"] = "";
$feed["roles"] = "";
$feed["title"] = "";
$feed["menu_type"] = "";
$feed["perm"] = "";
$feed["definition"] = array (
  'custom_cols' =>
  array (
    0 =>
    array (
      'label' => 'Edit',
      'weight' => '7',
      'code' => '<?php
  $confirm = \'\';
  if ($row[\'module\'] != \'\') {
    $confirm = \' onclick="if (!confirm(\\\'This feed was created by a module. Edit it at your own risk. It would be wise to make a backup of this feed before editing. Do you want to continue?\\\')) { return false;}" \';
  }
  ?>
  <a <?php echo $confirm; ?> href="<?php echo url(\'qviews/add_feed/\'. $row[\'id\']);?>">Edit</a>',
    ),
    1 =>
    array (
      'label' => 'Delete',
      'weight' => '8',
      'code' => '<?php
  echo qviews_col_delete($data, $definition, $row[\'id\'], $row)
  ?>',
    ),
    2 =>
    array (
      'label' => 'Export',
      'weight' => '6',
      'code' => '<a href="<?php echo url(\'qviews/export/\'. $row[\'pid\']);?>">Export</a>',
    ),
    3 =>
    array (
      'label' => 'Import',
      'weight' => '6',
      'code' => '<a href="<?php echo url(\'qviews/import/\'. $row[\'pid\']);?>?destination=qviews/list_feeds">Import</a>',
    ),
    4 =>
    array (
      'label' => 'Preview',
      'weight' => '6',
      'code' => '<a href="<?php echo url(\'qviews/preview/\'. $row[\'pid\']);?>">Preview</a>',
    ),
    5 =>
    array (
      'label' => 'Copy',
      'weight' => '6',
      'code' => '<a href="<?php echo url($_GET[\'q\']) . \'?action=copy&pid=\'. $row[\'pid\'];?>">Copy</a>',
    ),
  ),
  'rows' => '20',
  'cols' => '',
  'row_theme_function' => '',
  'grid_callback' => '',
  'example_args' => NULL,
  'sql' => 'SELECT * FROM qviews_feeds',
  'count_sql' => '',
  'num_pagination_links' => '10',
  'grid_theme_code' => '',
  'use_db_rewrite_sql' => 0,
  'javascript_on_refresh' => '',
  'node_delete_column' => 0,
  'node_edit_column' => 0,
  'user_delete_column' => 0,
  'user_edit_column' => 0,
  'comment_delete_column' => 0,
  'comment_edit_column' => 0,
  'no_ajax_pagination' => 0,
  'start_on_last' => 0,
  'scroll_to_top' => 0,
  'hide_if_single_page' => 0,
  'start_empty' => 0,
  'asc_on' => '',
  'asc_off' => '',
  'desc_on' => '',
  'desc_off' => '',
  'loading' => '',
  'loading_animation' => '',
  'loaded_animation' => '',
  'overall_theme_function' => '',
  'overall_theme_code' => '',
  'search_button_text' => '',
  'search_input_size' => '',
  'search_label' => '',
  'hide_clear_link' => 0,
  'page_footer' => '',
  'page_header' => '',
  'page_args' => '',
  'first_html' => '',
  'prev_html' => '',
  'next_html' => '',
  'last_html' => '',
  'theme_class' => '',
  'theme_id' => '',
  'block_names' => '',
  'no_headers' => 0,
  'empty_text' => '',
  'columns' =>
  array (
    'id' =>
    array (
      'col' => 'id',
      'sortable' => true,
      'display' => 1,
      'weight' => '-1',
      'prefix' => '',
      'orderby' => '1',
      'orderby_type' => 'desc',
      'label' => 'Id',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'pid' =>
    array (
      'col' => 'pid',
      'sortable' => true,
      'searchable' => true,
      'display' => 1,
      'weight' => '1',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Pid',
      'callback' => 'qviews_custom',
      'callback_args' => '<?php         if ($row[\'path\']) {        echo \'<a href="\'. url($row[\'path\']) .\'">\'. $row[\'pid\'] . \'</a>\';      } else {        echo $row[\'pid\'];      } ?> ',
    ),
    'definition' =>
    array (
      'col' => 'definition',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Definition',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'module' =>
    array (
      'col' => 'module',
      'sortable' => true,
      'display' => 1,
      'weight' => '3',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Module',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'description' =>
    array (
      'col' => 'description',
      'searchable' => true,
      'display' => 1,
      'weight' => '2',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Description',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'block' =>
    array (
      'col' => 'block',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Block',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'path' =>
    array (
      'col' => 'path',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Path',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'roles' =>
    array (
      'col' => 'roles',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Roles',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'title' =>
    array (
      'col' => 'title',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Title',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'menu_type' =>
    array (
      'col' => 'menu_type',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Menu Type',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'perm' =>
    array (
      'col' => 'perm',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Perm',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
  ),
);
$qviews_feeds_all[$feed["pid"]] = $feed;
unset($feed);


// Node browser for Q-Views Browser
$feed["pid"] = "Browse Nodes";
$feed["description"] = "Used by the Q-Views Browser to browse nodes.";
$feed["blocks"] = "0";
$feed["path"] = "";
$feed["module"] = "qviews";
$feed["roles"] = "";
$feed["title"] = "";
$feed["menu_type"] = "";
$feed["perm"] = "";
$feed["definition"] = array (
  'filters' =>
  array (
    0 =>
    array (
      'columns' => 'type',
      'comparison' => 'equal',
      'title' => 'Content type',
      'parameters' => '',
      'type' => 'select',
    ),
    1 =>
    array (
      'columns' => 'title',
      'comparison' => 'like',
      'title' => 'Title is like',
      'parameters' => '',
      'type' => 'text',
    ),
  ),
  'rows' => '20',
  'cols' => '',
  'row_theme_function' => '',
  'grid_callback' => '',
  'example_args' => '\'page\'',
  'sql' => 'SELECT DISTINCT(n.nid), n.*,  u.name FROM node n, users u WHERE u.uid = n.uid',
  'count_sql' => NULL,
  'num_pagination_links' => '10',
  'grid_theme_code' => '',
  'use_db_rewrite_sql' => 0,
  'node_delete_column' => 1,
  'node_edit_column' => 1,
  'user_delete_column' => 0,
  'user_edit_column' => 0,
  'comment_delete_column' => 0,
  'comment_edit_column' => 0,
  'no_ajax_pagination' => 0,
  'asc_on' => '',
  'asc_off' => '',
  'desc_on' => '',
  'desc_off' => '',
  'loading' => '',
  'loading_animation' => '',
  'loaded_animation' => '',
  'overall_theme_function' => '',
  'overall_theme_code' => '',
  'page_footer' => 'Footer content',
  'page_header' => '',
  'page_args' => '',
  'first_html' => '',
  'prev_html' => '',
  'next_html' => '',
  'last_html' => '',
  'theme_class' => '',
  'theme_id' => '',
  'block_names' => '',
  'columns' =>
  array (
    'nid' =>
    array (
      'col' => 'nid',
      'sortable' => true,
      'display' => 1,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Nid',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'vid' =>
    array (
      'col' => 'vid',
      'display' => 0,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Vid',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'type' =>
    array (
      'col' => 'type',
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Type',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'title' =>
    array (
      'col' => 'title',
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Title',
      'callback' => 'qviews_custom',
      'callback_args' => '<a href="<?php echo url(\'node/\'. $row[\'nid\']); ?>"><?php echo $value; ?></a>',
    ),
    'uid' =>
    array (
      'col' => 'uid',
      'display' => 1,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Uid',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'status' =>
    array (
      'col' => 'status',
      'display' => 1,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Published',
      'callback' => 'qviews_col_checkbox',
      'callback_args' => '\'status_col\', \'1\', \'0\', \'node\', \'status\', \'nid\', \'\'',
    ),
    'created' =>
    array (
      'col' => 'created',
      'display' => 1,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Created',
      'callback' => 'qviews_timestamp_to_time',
      'callback_args' => '\'Y-m-d\'',
    ),
    'changed' =>
    array (
      'col' => 'changed',
      'display' => 0,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Changed',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'comment' =>
    array (
      'col' => 'comment',
      'display' => 0,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Comment',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'promote' =>
    array (
      'col' => 'promote',
      'display' => 0,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Promote',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'moderate' =>
    array (
      'col' => 'moderate',
      'display' => 0,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Moderate',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'sticky' =>
    array (
      'col' => 'sticky',
      'display' => 0,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Sticky',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'name' =>
    array (
      'col' => 'name',
      'sortable' => true,
      'searchable' => true,
      'display' => 0,
      'weight' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Name',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
  ),
);
$qviews_feeds_all[$feed["pid"]] = $feed;
unset($feed);


// User Browser for Q-Views Browser
$feed["pid"] = "Browse Users";
$feed["module"] = "qviews";
$feed["description"] = "Used by the Q-Views Browser to browse users.";
$feed["blocks"] = "0";
$feed["path"] = "";
$feed["roles"] = "";
$feed["title"] = "";
$feed["menu_type"] = "";
$feed["perm"] = "";
$feed["definition"] = array (
  'rows' => '20',
  'cols' => '',
  'row_theme_function' => '',
  'grid_callback' => '',
  'example_args' => NULL,
  'sql' => 'SELECT * FROM users WHERE uid != 0',
  'count_sql' => NULL,
  'num_pagination_links' => '10',
  'grid_theme_code' => '',
  'use_db_rewrite_sql' => 0,
  'node_delete_column' => 0,
  'node_edit_column' => 0,
  'user_delete_column' => 1,
  'user_edit_column' => 1,
  'comment_delete_column' => 0,
  'comment_edit_column' => 0,
  'no_ajax_pagination' => 0,
  'asc_on' => '',
  'asc_off' => '',
  'desc_on' => '',
  'desc_off' => '',
  'loading' => '',
  'loading_animation' => '',
  'loaded_animation' => '',
  'overall_theme_function' => '',
  'overall_theme_code' => '',
  'page_footer' => '',
  'page_header' => '',
  'page_args' => '',
  'first_html' => '',
  'prev_html' => '',
  'next_html' => '',
  'last_html' => '',
  'theme_class' => '',
  'theme_id' => '',
  'block_names' => '',
  'no_headers' => 0,
  'columns' =>
  array (
    'uid' =>
    array (
      'col' => 'uid',
      'sortable' => true,
      'searchable' => true,
      'display' => 1,
      'weight' => '1',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Uid',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'name' =>
    array (
      'col' => 'name',
      'sortable' => true,
      'searchable' => true,
      'display' => 1,
      'weight' => '2',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Name',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'pass' =>
    array (
      'col' => 'pass',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Pass',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'mail' =>
    array (
      'col' => 'mail',
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Mail',
      'callback' => 'qviews_custom',
      'callback_args' => '<a href="mailto:<?php echo $value;?>"><?php echo $value;?></a>',
    ),
    'mode' =>
    array (
      'col' => 'mode',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Mode',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'sort' =>
    array (
      'col' => 'sort',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Sort',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'threshold' =>
    array (
      'col' => 'threshold',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Threshold',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'theme' =>
    array (
      'col' => 'theme',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Theme',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'signature' =>
    array (
      'col' => 'signature',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Signature',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'created' =>
    array (
      'col' => 'created',
      'sortable' => true,
      'display' => 1,
      'weight' => '4',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Created',
      'callback' => 'qviews_format_interval',
      'callback_args' => '2',
    ),
    'access' =>
    array (
      'col' => 'access',
      'sortable' => true,
      'display' => 0,
      'weight' => '3',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Last access',
      'callback' => 'qviews_format_interval',
      'callback_args' => '2',
    ),
    'login' =>
    array (
      'col' => 'login',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Login',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'status' =>
    array (
      'col' => 'status',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Status',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'timezone' =>
    array (
      'col' => 'timezone',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Timezone',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'language' =>
    array (
      'col' => 'language',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Language',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'picture' =>
    array (
      'col' => 'picture',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Picture',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'init' =>
    array (
      'col' => 'init',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Init',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'data' =>
    array (
      'col' => 'data',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Data',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'timezone_name' =>
    array (
      'col' => 'timezone_name',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Timezone Name',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
  ),
);
$qviews_feeds_all[$feed["pid"]] = $feed;
unset($feed);

// Comment browser for qviews browser
$feed["pid"] = "Browse Comments";
$feed["description"] = "Used by the Q-Views Browser to browse comments.";
$feed["blocks"] = "0";
$feed["module"] = "qviews";
$feed["path"] = "";
$feed["roles"] = "";
$feed["title"] = "";
$feed["menu_type"] = "";
$feed["perm"] = "";
$feed["definition"] = array (
  'rows' => '20',
  'cols' => '',
  'row_theme_function' => '',
  'grid_callback' => '',
  'example_args' => NULL,
  'sql' => 'SELECT * FROM comments c, node n WHERE c.nid = n.nid',
  'count_sql' => '',
  'num_pagination_links' => '10',
  'grid_theme_code' => '',
  'use_db_rewrite_sql' => 0,
  'node_delete_column' => 0,
  'node_edit_column' => 0,
  'user_delete_column' => 0,
  'user_edit_column' => 0,
  'comment_delete_column' => 0,
  'comment_edit_column' => 0,
  'no_ajax_pagination' => 0,
  'scroll_to_top' => 0,
  'asc_on' => '',
  'asc_off' => '',
  'desc_on' => '',
  'desc_off' => '',
  'loading' => '',
  'loading_animation' => '',
  'loaded_animation' => '',
  'overall_theme_function' => '',
  'overall_theme_code' => '',
  'page_footer' => '',
  'page_header' => '',
  'page_args' => '',
  'first_html' => '',
  'prev_html' => '',
  'next_html' => '',
  'last_html' => '',
  'theme_class' => '',
  'theme_id' => '',
  'block_names' => '',
  'no_headers' => 0,
  'columns' =>
  array (
    'cid' =>
    array (
      'col' => 'cid',
      'sortable' => true,
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Cid',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'pid' =>
    array (
      'col' => 'pid',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Pid',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'nid' =>
    array (
      'col' => 'nid',
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'prefix' => 'n',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Nid',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'uid' =>
    array (
      'col' => 'uid',
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'prefix' => 'n',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Uid',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'subject' =>
    array (
      'col' => 'subject',
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Subject',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'comment' =>
    array (
      'col' => 'comment',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Comment',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'hostname' =>
    array (
      'col' => 'hostname',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Hostname',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'timestamp' =>
    array (
      'col' => 'timestamp',
      'sortable' => true,
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Timestamp',
      'callback' => 'qviews_format_interval',
      'callback_args' => '1',
    ),
    'score' =>
    array (
      'col' => 'score',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Score',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'status' =>
    array (
      'col' => 'status',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Status',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'format' =>
    array (
      'col' => 'format',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Format',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'thread' =>
    array (
      'col' => 'thread',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Thread',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'users' =>
    array (
      'col' => 'users',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Users',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'name' =>
    array (
      'col' => 'name',
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Name',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'mail' =>
    array (
      'col' => 'mail',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Mail',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'homepage' =>
    array (
      'col' => 'homepage',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Homepage',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'vid' =>
    array (
      'col' => 'vid',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Vid',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'type' =>
    array (
      'col' => 'type',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Type',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'title' =>
    array (
      'col' => 'title',
      'searchable' => true,
      'display' => 1,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Title',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'created' =>
    array (
      'col' => 'created',
      'display' => 1,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Created',
      'callback' => 'qviews_timestamp_to_time',
      'callback_args' => '\'Y-m-d\'',
    ),
    'changed' =>
    array (
      'col' => 'changed',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Changed',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'promote' =>
    array (
      'col' => 'promote',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Promote',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'moderate' =>
    array (
      'col' => 'moderate',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Moderate',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
    'sticky' =>
    array (
      'col' => 'sticky',
      'display' => 0,
      'weight' => '',
      'prefix' => '',
      'orderby' => '',
      'orderby_type' => '',
      'label' => 'Sticky',
      'callback' => '',
      'callback_args' => 'Add your arguments here',
    ),
  ),
);
$qviews_feeds_all[$feed["pid"]] = $feed;
unset($feed);

