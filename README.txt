
Q-Views module:
-------------------------
Author - Chris Shattuck (www.impliedbydesign.com)
License - GPL


Overview:
-------------------------
The main goal of the Q-Views module is to provide a powerful, simple and flexible method by which site manager can create lists, or 'feeds' of content. Q-Views is highly flexible, and can be used in many different ways. Below are a few of the features:

- Create ajax-refreshed, sortable, searchable tables without any coding
- Display data in a table, as a grid, or supply your own simple code to create another kind of list.
- Base tables on node data, or draw data from any other table with a custom SQL query.
- Edit data in-line without any page refreshing
- Customize any aspect of the feed with simple CSS
- A feed can produce any number of blocks. Pass varying arguments to the different blocks to produce varying lists of content.
- Each feed can have it's own custom URL and get added to the menu system
- Use static pagination instead of ajax-based for search engine friendliness
- Modules can hook into Q-Views to supply administrative or front-end feeds. These feeds can then be edited by users to include additional data or theming. If the feeds get's mangled, there is a built in restore function.
- Import / export feeds for easy addition to a module installation or to create backups.
- Add filters to help users find the data they want fast. Types of filters include date, checkbox, select and text, and several kinds of operators, including a range.
- One easy-to-use form for editing / creating feeds.
- Advanced tools available for creating custom columns or content theming.


Quick comparison to the "Views" module
-------------------------
The Views module is highly popular, widely supported well integrated into many modules. The framework is smart, and is accessible for new and veteran Drupal users. Q-Views has the same basic goal: to provide a UI for creating lists of content, but takes a different approach that is incompatible with Views, namely creating feeds of content based on direct queries.

In Q-Views, one starts the process of building a feed with the database query. Modules can hook in to Q-Views to supply starter queries, but you are free to manipulate that query however you'd like before theming the results. While this requires some comfort with SQL, it also makes Q-Views flexible enough to be used with any kind of data in your database. Once Q-Views has validated the query, you are supplied several different options regarding how the feed will display.


The basic pages
-------------------------
Once Q-Viewss has been installed, it will add two items to the menu:

Q-Views: A page where you can manage existing feeds, import or add new ones. Q-Viewss uses itself to build out the table you see here.

Content browser: The content browser page has a dual-purpose. First, it provides an useful example of the flexibility and simplicity of Q-Views. Secondly, it provides a powerful replacement for the "admin/content/node" page, as well as a quick browser for users and comments.


Couple a' Use Cases
-------------------------
Case: Create a table to administer nodes quickly, which includes an in-line ajax-enabled checkbox for setting the published status of a node. 

Use: Select which columns you want to display, set the "status" column callback to "qviews_col_checkbox", follow the brief instructions for setting the parameters, and you're done.
---------
Case: Create a grid of gallery images that can be browsed via ajax. 

Use: This would require a couple lines of HTML code, a call to the imagecache module to generate a thumbnail image, and some css to make it look pretty. All of that can go into the "Item theme code" box and you're done.
---------
Case: Create a block of the most recently added nodes, that switched content type depending on the second parameter in the URL. 

Use: Create a vanilla Q-Views Feed with an argument in the query for the node "type". Set the number of blocks to 1. Set the block visibility by visiting the block admin page, and set the arguments to "%arg1". That's it.



